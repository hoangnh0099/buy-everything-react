import React, { Component } from 'react';
import './Search.css';
import { Link } from 'react-router-dom';
import axios from 'axios';
import queryString from 'query-string';

class Search extends Component {
  constructor() {
    super();
    this.state = {
      products: []
    }
  }

  componentDidMount = () => {
    axios.get('/api/products')
      .then(result => {
        this.setState({
          products: result.data
        })
      });
  }

  render() {
    const searchQuery = queryString.parse(this.props.location.search);
    const searchResult = this.state.products.filter(product => {
      return product.name.toLowerCase().indexOf(searchQuery.result.toLocaleLowerCase()) !== -1;
    })

    return (
      <div className="Search">
        {
          searchResult.map((product, index) => {
            return (
              <ul key={index}>
                <li>
                  <Link to={`/detail/${product._id}`}>{product.name}</Link>
                  <p>{product.price}</p>
                </li>
              </ul>
            );
          })
        }
      </div>
    );
  }
}

export default Search;