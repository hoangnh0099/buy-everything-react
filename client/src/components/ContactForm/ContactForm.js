import React, { Component } from 'react';
import './ContactForm.css';

class ContactForm extends Component {
  constructor() {
    super();
    this.state = {
      fullName: "",
      address: "",
      email: "",
      phoneNumber: "",
      quantity: 1
    }
  }

  handleChange = (event) => {
    this.setState({
      [event.target.name]: event.target.value
    })
  }

  formatCurrency = (price) => {
    return price.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1.");
  }

  render() {
    return (
      <div className="ContactForm">
        <form method="POST" action="http://localhost:8888/api/products/buy">
          <input type="text" name="productName" value={this.props.productName} />
          <input type="text" name="productPrice" value={this.formatCurrency(this.props.price)} />
          <input type="text" name="total" value={this.formatCurrency(parseInt(this.props.price) * this.state.quantity)} />
          <input type="text" placeholder="Full Name" name="fullName" value={this.state.fullName} onChange={this.handleChange} required />
          <input type="text" placeholder="Address" name="address" value={this.state.address} onChange={this.handleChange} required />
          <input type="email" placeholder="Email" name="email" value={this.state.email} onChange={this.handleChange} required />
          <input type="text" placeholder="Phone number" name="phoneNumber" value={this.state.phoneNumber} onChange={this.handleChange} required />
          <input type="number" placeholder="Quantity" name="quantity" value={this.state.quantity} onChange={this.handleChange} required />
          <button className="buy-button">Buy</button>
        </form>
      </div>
    );
  }
}

export default ContactForm;