import React, { Component } from 'react';
import './Detail.css';
import axios from 'axios';

import ContactForm from './../ContactForm/ContactForm';

class Detail extends Component {
  constructor() {
    super();
    this.state = {
      products: [],
      isShowForm: false,
    }
  }

  componentDidMount = () => {
    axios.get('/api/products').then(result => {
      this.setState({ products: result.data });
    })
  }

  toggleForm = () => {
    this.setState({
      isShowForm: !this.state.isShowForm
    })
  }

  formatCurrency = (price) => {
    return price.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1.");
  }

  render() {
    const id = this.props.match.params.id;
    return (
      <div className="Detail">
        {
          this.state.products.map((product, index) => {
            if (id === product._id) {
              return (
                <div className="detail-product" key={index}>
                  <div className="detail-product-top">
                    <img src={product.thumbnail} alt={product.name} />
                    <div className="detail-product-top-information">
                      <h1>{product.name}</h1>
                      <p>Giá: {this.formatCurrency(product.price)} đ</p>
                      <button onClick={this.toggleForm} className="buy-button">Buy now</button>
                      {this.state.isShowForm ? <ContactForm productName={product.name} price={product.price} /> : ""}
                    </div>
                  </div>
                  <br />
                  <br />
                  <h1>Thông tin sản phẩm</h1>
                  <br />
                  <div dangerouslySetInnerHTML={{ __html: product.detail }} className="detail-product" />
                </div>
              )
            }
          })
        }
      </div>
    );
  }
}

export default Detail;