import React, { Component } from 'react';
import './Home.css';
import axios from 'axios';

// Component
import Card from './../Card/Card';

class Home extends Component {
  constructor() {
    super();
    this.state = {
      products: []
    }
  }

  componentDidMount = () => {
    axios.get('/api/products').then(result => {
      this.setState({ products: result.data });
    })
  }

  render() {
    return (
      <div className="Home">
        {
          this.state.products.map((product, index) => {
            return (
              <Card 
                id={product._id}
                name={product.name} 
                thumbnail={product.thumbnail} 
                price={product.price}
                key={index} />
            );
          })
        }
      </div>
    );
  }
}

export default Home;