import React, { Component } from 'react';
import './Card.css';
import { Link } from 'react-router-dom';

class Card extends Component {
  formatCurrency = (price) => {
    return price.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1.");
  }

  render() {
    return (
      <div className="Card">
        <img src={this.props.thumbnail} alt={this.props.name} />
        <h4><Link to={`/detail/${this.props.id}`}>{this.props.name}</Link></h4>
        <br />
        <p>{this.formatCurrency(this.props.price)} đ</p>
      </div>
    );
  }
}

export default Card;