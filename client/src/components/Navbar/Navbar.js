import React, { Component } from 'react';
import './Navbar.css';
import { BrowserRouter, Route, NavLink, Link, Switch } from 'react-router-dom';

// Component
import Home from './../Home/Home';
import Search from './../Search/Search';
import Detail from './../Detail/Detail';

class Navbar extends Component {
  render() {
    return (
      <BrowserRouter>
        <div className="Navbar">
          <div className="brand-logo">
            <Link to="/">Buy Everything</Link>
          </div>

          <nav>
            <NavLink to="/" exact>Home</NavLink>
            <form method="GET" action="/search">
              <input type="text" placeholder="Search" name="result" />
            </form>
          </nav>
        </div>

        <Switch>
          <Route path="/" exact component={Home} />
          <Route path="/search" exact component={Search} />
          <Route path="/detail/:id" exact component={Detail} />
        </Switch>
      </BrowserRouter>
    );
  }
}

export default Navbar;