const express = require('express');
const bodyParser = require('body-parser');
const cookieParser = require('cookie-parser');
const adminRouter = require('./routes/admin.route');
const apiRouter = require('./routes/api.route');
const app = express();
require('dotenv').config();
require('./models/connection.models');

app.set('views', './views');
app.set('view engine', 'ejs');

// Middleware
app.use(cookieParser());
app.use(express.static('public'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(adminRouter);
app.use(apiRouter);

const PORT = process.env.PORT;

app.listen(PORT, () => console.log(`Server is running on port ${PORT}`));