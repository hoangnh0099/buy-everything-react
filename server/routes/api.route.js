const express = require('express');
const router = express.Router();
const Bill = require('./../models/bill.models');

// Controller
const productsController = require('./../controllers/api-controller/products.controller');

// Routes
router.get('/api/products', productsController.products);

router.post('/api/products/buy', function (req, res) {
  const data = req.body;
  console.table(data);

  const newBill = new Bill({
    customer: data.fullName,
    address: data.address,
    email: data.email,
    phoneNumber: data.phoneNumber,
    productName: data.productName,
    productPrice: parseInt(data.productPrice),
    quantity: parseInt(data.quantity),
    total: parseInt(data.total),
  });

  newBill.save().then(function () {
    res.redirect("http://localhost:3000");
  });
});

module.exports = router;