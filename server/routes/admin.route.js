const express = require('express');
const router = express.Router();
const Product = require('./../models/product.models');
const Bill = require('./../models/bill.models');
const authMiddleware = require('./../middleware/auth.middleware');

// Controller

// Routes
router.get('/admin', authMiddleware.authLogin, function(req, res) {
  Bill.find().then(function(result) {
    res.render('index', { result });
  })
})

router.get('/admin/login', function(req, res) {
  res.render('login');
})

router.post('/admin/login', function(req, res) {
  const username = req.body.username;
  const password = req.body.password;

  // console.log(`${username} - ${password}`);

  
  if (username === "admin" && password === "123456") {
    res.cookie("adminId", "abcxyz");
    res.redirect('/admin');
  } else {
    res.redirect('/admin/login');
  }
})

router.get('/admin/add-product', authMiddleware.authLogin, function(req, res) {
  res.render('add-product');
})

router.post('/admin/add-product', function(req, res) {
  const newProduct = new Product({
    name: req.body.productName,
    price: req.body.productPrice,
    thumbnail: req.body.productThumbnail,
    detail: req.body.productDetail,
  })

  newProduct.save().then(function() {
    res.redirect('/admin/manage-product');
  });
})

router.get('/admin/manage-product', authMiddleware.authLogin, function(req, res) {
  Product.find().then(function(result) {
    res.render('manage-product', { products: result });
  })
})

router.get('/admin/delete/:id', authMiddleware.authLogin, function(req, res) {
  const id = req.params.id;
  Product.findByIdAndDelete(id).then(function() {
    res.redirect('/admin/manage-product');
  })
})

router.get('/admin/edit/:id', authMiddleware.authLogin, function(req, res) {
  const id = req.params.id;
  Product.findById(id).then(function(result) {
    res.render('edit-product', { product: result });
  })
})

router.post('/admin/edit/:id', function(req, res) {
  const id = req.params.id;

  const newData = {
    name: req.body.productName,
    price: req.body.productPrice,
    thumbnail: req.body.productThumbnail,
    detail: req.body.productDetail,
  }
  
  Product.findByIdAndUpdate(id, newData, function(err, result) {
    console.log(id);
    res.redirect('/admin/manage-product');
  });
})


module.exports = router;