const Product = require('./../../models/product.models');

module.exports.products = function(req, res) {
  Product.find().then(function(result) {
    res.json(result);
  })
}