const mongoose = require('mongoose');

const billSchema = mongoose.Schema({
  customer: String,
  address: String,
  email: String,
  phoneNumber: String,
  productName: String,
  productPrice: Number,
  quantity: Number,
  total: Number
});

const Bill = mongoose.model("bill", billSchema);
module.exports = Bill;
