module.exports.authLogin = function(req, res, next) {
  console.log("Auth Middleware");

  if (!req.cookies.adminId) {
    res.redirect('/admin/login');
    return
  }

  // if (!req.signedCookies.adminId) {
  //   res.redirect('/admin/login');
  // }

  // if (req.signedCookies.adminId !== "abcxyz") {
  //   res.redirect('/admin/login');
  // }

  next();
}